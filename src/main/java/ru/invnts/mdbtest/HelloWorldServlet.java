package ru.invnts.mdbtest;

import java.io.IOException;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;;

// request url: "/?msg=Hello%20world!"
@WebServlet("/")
public class HelloWorldServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String msg = req.getParameter("msg");
		System.out.println(String.format("Sending message \"%s\" from the servlet!", msg));
		sendMessage(msg);
	}

	void sendMessage(String msg) {

		Context context;
		ConnectionFactory connectionFactory;
		Queue queue;
		Connection connection = null;
		try {
			context = new InitialContext();
			connectionFactory = (ConnectionFactory) context.lookup("java:/ConnectionFactory");
			queue = (Queue) context.lookup("java:/activemq/queue/TestQueue");
			connection = connectionFactory.createConnection();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			MessageProducer publisher = session.createProducer(queue);

			connection.start();

			TextMessage message = session.createTextMessage(msg);
			publisher.send(message);

			connection.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (JMSException e) {
					throw new RuntimeException(e);
				}
			}
		}
	}

}
