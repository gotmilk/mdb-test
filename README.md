# Simple Web Application

## Includes Message Driven Bean

As for the spec it is not required for WAR to have the full EJB API. Although, this will definitely work on Wildfly 10. It hasn't been tested with others and is not recommended to use EJB as a part of WAR archive.

**Don't forget to configure the queue for your JMS provider:**

* destinationType = javax.jms.Queue
* destination = java:/activemq/queue/TestQueue